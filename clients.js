const socketURL = "ws://127.0.0.1:4242";

class Client {
    constructor() {
        let controller = new NetworkController(clients.length, () => {
            console.log("Snake " + controller.uid + " spawned.");
        });
    }
}

class PlayerClient extends Client {

}

class GameClient {
    constructor(ready) {
        let clients = [];
        let socket = new WebSocket(socketURL);

        this.playerSnake = null;
        this.snakeLocations = [];

        socket.onmessage = (e => {
            let packet = e.data.substring(1, e.data.indexOf("]"));

            let args = e.data.replace("[" + packet + "]","").split(",");

            this.selfSnakeId = args[0];

            if(packet == PacketCodes.READY) {
                let selfColor = color(args[1]);
                let selfController = new PlayerController(this.selfSnakeId);
                this.playerSnake = new Snake(selfController, selfColor);
                for(let i = 2; i < args.length; i+=2) {
                    let networkController = new NetworkController(i, () => {
                        console.log("New snake registered");
                    });

                    clients[i / 2] = new Snake(networkController, color(args[i + 1]));
                }
                clients[0] = this.playerSnake;
                ready();
            } else {
                if(packet == PacketCodes.BAD_COLOR) {
                    socket.send("[" + PacketCodes.CONNECT + "]red");
                    // alert("The color you have chosen has already been taken!");
                    return;
                } else if(packet == PacketCodes.NEW_CLIENT) {
                    let networkController = new NetworkController(args[0], () => {
                        console.log("New snake registered");
                    });

                    clients[args[0]] = new Snake(networkController, color(args[1])); 
                    return;
                } else if(packet == PacketCodes.SNAKE_MOVE) {
                    
                }
            }
        });

        socket.onerror = (e => {
            console.dir(e);
        });
        socket.onclose = (e => {
            console.log("GameClient disconnected from server.");
        });

        socket.onopen = (e => {
            console.log("GameClient connected to server.");
            socket.send("[" + PacketCodes.CONNECT + "]magenta");
        });

        

        this.clients = clients;
        this.socket = socket;
    }

    drawSnakes() {
        this.clients.forEach(snake => {
            fill(snake.color);
            snake.locations.forEach(location => {
                let row = Math.floor(location / cellCount);
                let column = location % cellCount;

                let posY = row * cellSize;
                let posX = column * cellSize;

                rect(posX, posY, cellSize, cellSize);
            });
        });
    }
}