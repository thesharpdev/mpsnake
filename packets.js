class PacketCodes {
    static READY = "READY";
    static SNAKE_MOVE = "MOVE";
    static SNAKE_LEFT = "LEFT";
    static SNAKE_RIGHT = "RIGHT";
    static FOOD_COLLECT = "COLLECT";
    static SNAKE_SPAWNED = "NEWSNAKE";
    static SNAKE_DIED = "SNAKEDIED";
    static FOOD_SPAWNED = "NEWFOOD";
    static HEARTBEAT = "HB";
    static CONNECT = "CONNECT";
    static BAD_COLOR = "BAD_COLOR"
    static NEW_CLIENT = "NEW_CLIENT";
}