class Snake {
    constructor(controller, color) {

        controller.oneat = (e => {

        });

        controller.onrotateleft = (e => {

        });

        controller.onrotateright = (e => {

        });

        controller.onmove = () => {
            let newPosition = 0;
            let currentPosition = this.locations[this.locations.length - 1]
            if(this.direction == "N") {
                if(currentPosition < cellCount) {
                    newPosition = currentPosition + (cellCount * (cellCount - 1));
                } else {
                    newPosition = currentPosition - cellCount;
                }
            } else if(this.direction == "E") {
                if(currentPosition % cellCount == (cellCount - 1)) {
                    newPosition = currentPosition - cellCount + 1;
                } else {
                    newPosition = currentPosition + 1;
                }
            } else if(this.direction == "S") {
                if(currentPosition >= (cellCount * (cellCount - 1))) {
                    newPosition = currentPosition - (cellCount * (cellCount - 1));
                } else {
                    newPosition = currentPosition + cellCount;
                }
            } else if(this.direction == "W") {
                if(currentPosition % cellCount == 0) {
                    newPosition = currentPosition + cellCount - 1;
                } else {
                    newPosition = currentPosition - 1;
                }
            }

            this.locations.push(Math.floor(newPosition));
            this.locations.shift();

            // console.log(this.locations);
            // console.log(this.direction);
        };

        controller.ondie = (e => {

        });

        controller.onspawn = (e => {

        });

        this.direction = "W";
        this.locations = [];
        this.controller = controller;
        this.color = color;
    }
}