const cellSize = 28;
const cellCount = 32;

var gameClient = new GameClient(() => {
    let playerSpawnLoc = Math.floor(Math.random() * (cellCount * cellCount));
    gameClient.playerSnake.direction = "E";
    gameClient.playerSnake.locations = [playerSpawnLoc - 5, playerSpawnLoc - 4, playerSpawnLoc - 3, playerSpawnLoc - 2, playerSpawnLoc - 1, playerSpawnLoc];
    setInterval(moveLoop, 100);
});

function setup() {
    let size = cellSize * cellCount;
    createCanvas(size, size);
}

function draw() {
    drawGrid();
}

function drawGrid() {
    background(0);
    strokeWeight(1);
    for (let i = 0; i < cellCount * cellCount; i++) {
        let row = Math.floor(i / cellCount);
        let column = i % cellCount;

        let posY = row * cellSize;
        let posX = column * cellSize;

        let nextPosX = (column + 1) * cellSize;
        let nextPosY = (row + 1) * cellSize;

        stroke(0, 200, 180);

        line(posX, posY, nextPosX, posY);
        line(posX, posY, posX, nextPosY);

        fill(255);
        textSize(8);
        textAlign(LEFT, TOP);
        text(i, posX, posY);
    }
    line(width, 0, width, height);
    line(0, height, width, height);

    gameClient.drawSnakes();
}

function moveLoop() {
    gameClient.playerSnake.controller.onmove();
}

function keyPressed() {
    let currentDirection = gameClient.playerSnake.direction;
    if(keyCode === LEFT_ARROW) {
        if(currentDirection != "E") {
            gameClient.playerSnake.direction = "W";
        }
    } else if(keyCode === RIGHT_ARROW) {
        if(currentDirection != "W") {
            gameClient.playerSnake.direction = "E";
        }
    } else if(keyCode === UP_ARROW) {
        if(currentDirection != "S") {
            gameClient.playerSnake.direction = "N";
        }
    } else if(keyCode === DOWN_ARROW) {
        if(currentDirection != "N") {
            gameClient.playerSnake.direction = "S";
        }
    }
}