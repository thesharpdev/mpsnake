const packets = require("./nodePackets.js");

const ws = require("nodejs-websocket");

let colors = [];

let heartbeatResponses = [];

var socketServer = ws.createServer(conn => {
    console.log("Connection from " + conn.socket.remoteAddress + ":" + conn.socket.remotePort);
    conn.on("text", str => {
        let packet = str.substring(1, str.indexOf("]"));

        let args = str.replace("[" + packet + "]","").split(",");
        
        if(packet == packets.CONNECT) {
            var newId = colors.length;
            var color = args[0];
            if(colors.includes(color)) {
                conn.sendText("[" + packets.BAD_COLOR + "]");
                return;
            }

            var clientsString = "";
            for(let i = 0; i < colors.length; i++) {
                let remColor = colors[i];
                if(i == 0) {
                    clientsString += `${i},${remColor}`;
                } else {
                    clientsString += `,${i},${remColor}`;
                }
            }

            let comma = clientsString == "" ? "" : ",";

            colors[newId] = color;
            console.log(`Sending [${packets.READY}]${newId},${color}${comma}${clientsString} to ${conn.socket.remoteAddress}:${conn.socket.remotePort}`);
            conn.sendText(`[${packets.READY}]${newId},${color}${comma}${clientsString}`);
            socketServer.connections.forEach(element => {
                if(element != conn) element.sendText(`[${packets.NEW_CLIENT}]${newId},${color}`);
            });
        } else if(packet == packets.HEARTBEAT) {
            var clientId = args[0];
            heartbeatResponses.push(clientId);
        }
    });

    conn.on("close", (code, reason) => {
        console.log("Connection closed.");
    });

    conn.on("error", err => {
        console.error(err);
    });
}).listen(4242);